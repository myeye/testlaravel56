<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//分享功能
Route::group(['namespace'=>'Demo', 'prefix' => 'share'], function(){
    Route::get('/qrcode_output', 'ShareController@qrcodeOutput');  //生成url二维码
    Route::any('/wechat_valid', 'ShareController@wechatValid');  //微信服务器验证及消息处理返回
    Route::get('/wechat_get_jssdk', 'ShareController@getJssdk');  //获取使用jssdk需要的微信公众号配置信息
});
//测试小功能
Route::group(['namespace'=>'Demo', 'prefix' => 'demo'], function(){
    Route::get('/index1', 'DemoController@index1');  //测试返回json
    Route::get('/index2', 'DemoController@index2');  //测试检测参数
    Route::get('/index3', 'DemoController@index3');  //测试配置信息
    Route::get('/index4', 'DemoController@index4');  //测试uuid方法
    Route::get('/index5', 'DemoController@index5');  //测试请求
    Route::get('/index6', 'DemoController@index6');  //测试响应
    Route::get('/index7', 'DemoController@index7');  //测试url
    Route::get('/index8', 'DemoController@index8');  //测试session
    Route::get('/index9', 'DemoController@index9');  //测试异常

    Route::get('/index10', 'DemoController@index10');  //测试异常

    Route::get('/test_bankcard', 'DemoController@testBankcard');  //测试银行卡依赖

    Route::get('/video_to_base64', 'DemoController@videoToBase64');  //测试base64视频
});
//ORM测试
Route::group(['namespace'=>'Demo', 'prefix' => 'orm'], function(){
    Route::get('/has_one', 'OrmController@testHasOne');  //测试一对一关联操作
    Route::get('/belongs_to', 'OrmController@testBelongsTo');  //测试一对一关联操作

});
//cache测试
Route::group(['namespace'=>'Demo', 'prefix' => 'cache'], function(){
    Route::get('/index', 'CacheController@index');  //测试读取cache

});

//redis测试
Route::group(['namespace'=>'Demo', 'prefix' => 'redis'], function(){
    Route::get('/add_list', 'RedisController@addList');  //测试设置redis list
    Route::get('/get_list', 'RedisController@getList');  //测试读取redis list
});

//测试api路由
Route::match(['post','put'],'foo',function(){
    return 'api hello world';
});

//表单验证测试
Route::group(['namespace'=>'Demo', 'prefix' => 'vali'],function(){
    Route::get('/index', 'ValidateController@index');  //测试表单请求类
    Route::get('/index1', 'ValidateController@index1');  //方法内表单验证
});

//微信
Route::group(['namespace'=>'Wechat','prefix' => 'wechat'],function($router){
    Route::group(['prefix' => 'menu'], function ($router) {
        $router->get('/list', 'WechatController@menuList');
        $router->post('/create', 'WechatController@menuCreate');
        $router->post('/delete', 'WechatController@menuDelete');
    });
    Route::group(['prefix' => 'material'], function ($router) {
        $router->get('/list', 'WechatController@materialGet');  //永久素材列表
    });
});

//用户认证
Route::group(['namespace' => 'Auth','prefix' => 'auth'],function(){
    Route::get('/index',function (){
        return '2222';
    });
    Route::post('/register','RegisterController@register');  //注册用户
    Route::post('/login','LoginController@login');  //登录
    Route::get('/logout','LoginController@logout');  //退出
    Route::group(['middleware' => 'auth:user'],function(){
        Route::get('/userinfo','LoginController@userInfo');  //用户信息
    });
});

