<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//测试web路由
Route::get('foo',function(){   //基本路由
    return 'hello world';
});
Route::redirect('/hoo','/foo',301);  //重定向路由
Route::view('/welcome','welcome',['name' => 'zhang']);   //视图路由
Route::get('user/{id}',function($id){  //路由参数
    return 'User '.$id;
});
Route::get('/profile/{$id}','Demo\DemoController');
//路由组
Route::middleware(['first','second'])->group(function(){   //中间件
        Route::get('/test_middle',function(){
            return 'test middleware';
        });
});
//资源路由
Route::resource('photos', 'PhotoController', ['only' => [
    'index', 'show'
]]);

//命名空间路由组
Route::group(['namespace'=>'Demo','prefix'=>'view'],function(){
    Route::get('/index', 'TestviewController@index');  //测试视图
});

//表单验证机制详解
Route::group(['namespace'=>'Demo','prefix'=>'view'],function(){
    Route::get('/create', 'TestviewController@create');  //表单提交页面
    Route::post('/store', 'TestviewController@store');  //表单提交处理
});

//微信
Route::group(['namespace'=>'Wechat','prefix'=>'wechat'],function(){
    Route::any('/index', 'WechatController@serve');  //微信服务端认证和接消息
});
Route::group(['middleware' => ['wechat.oauth:default,snsapi_userinfo']], function () {
    Route::get('/wechat/user', function () {
        $user = session('wechat.oauth_user.default'); // 拿到授权用户资料

        var_dump($user);exit;
    });
});

//主页
Route::get('home',function (){
    return 'have login';
});
