<?php

namespace App\Http\Model\Demo;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    protected $table = 'assets';
    public function user(){
        return $this->belongsTo(User::class,'uid','id');
    }
}
