<?php

namespace App\Http\Model\Demo;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    //资产附表
    public function assets(){
        return $this->hasOne(Assets::class,'uid','id');
    }
}
