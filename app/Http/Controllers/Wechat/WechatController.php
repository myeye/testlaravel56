<?php

/*
 * 微信模块
 */
namespace App\Http\Controllers\Wechat;
use App\Http\Controllers\Common\BaseController;
use EasyWeChat\Kernel\Messages\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
/**
 * Description of WechatController
 * Date 2019年3月28日 15:50:07
 * @author Carter
 */
class WechatController extends BaseController{
     /**
     * 处理微信的请求消息
     *
     * @return string
     */
    public function serve()
    {
        Log::info(file_get_contents('php://input')); # 注意：Log 为 Laravel 组件，所以它记的日志去 Laravel 日志看，而不是 EasyWeChat 日志
        
        $app = app('wechat.official_account');
        $app->server->push(function($message){
            Log::info('message信息'.json_encode($message,JSON_UNESCAPED_UNICODE));
            switch ($message['MsgType']){
                case 'text':
                    if($message['Content'] == '测试回复'){
                        return new Media('VuYIgUJnK8VzdH5UEL48wEMiLWNHi5uSeacBDrKRoqM','mpnews');
                    }
                    return $message['Content'];
                    break;
                default:
                    return 'hello';
                    break;
            }
            return "欢迎关注 overtrue！";
        });

        return $app->server->serve();
    }

    //****************************************** 菜单 ********************************

    /**
     * 菜单列表
     * @return array|mixed
     */
    public function menuList()
    {
        $app = app('wechat.official_account');

        $list = $app->menu->list();

        return $this->returnData(['$_0', compact('list')]);
    }

    /**
     * 菜单创建
     * @param Request $request
     * @return array|mixed
     */
    public function menuCreate(Request $request)
    {
        $res = 'not json request';
        if ($request->isJson()) {
            $buttons = $request->input('buttons', null);
            $app = app('wechat.official_account');
            $res = $app->menu->create($buttons);
        }

        return $this->returnData(['$_0', compact('res')]);
    }

    /**
     * 菜单删除
     * @param Request $request
     * @return array|mixed
     */
    public function menuDelete(Request $request)
    {
        $menuId = $request->input('menuId', null);
        $app = app('wechat.official_account');
        $res = $app->menu->delete($menuId);  //不存在菜单id时全部删除

        return $this->returnData(['$_0', compact('res')]);
    }


    /**
     *获取永久素材列表
     * $type 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     * $offset 从全部素材的该偏移位置开始返回，可选，默认 0，0 表示从第一个素材
     * $count 返回素材的数量，可选，默认 20, 取值在 1 到 20 之间
     */
    public function materialGet(Request $request){
        //参数处理
        $type = $request->input('type', null);
        $offset = $request->input('offset', 0);
        $count = $request->input('count', null);

        //请求
        $app = app('wechat.official_account');
        $res = $app->material->list($type, $offset, $count);

        return $this->returnData(['$_0', compact('res')]);
    }
}
