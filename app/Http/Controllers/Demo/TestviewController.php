<?php

/*
 * 测试视图
 */

namespace App\Http\Controllers\Demo;
use App\Http\Controllers\Common\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Validator;
/**
 * Description of TestviewController
 * Date 2019年3月1日 20:12:48
 * @author Carter
 */
class TestviewController extends BaseController{
    //测试视图
    public function index(){
//        if(View::exists('test.index')){  //判断视图文件是否存在
//            return view('test.index')->with('name','carter');
//        }else{
//            return '404 view not found';
//        }
        //创建第一个可用视图，有数组中的第一个模版就用第一个，没有就依次往下，相当于exists的if else
        return view()->first(['custom.admin','test.index'],['name'=>'xiaofan']);
    }
    //表单提交页面
    public function create(){
        abort(404,'hahha');  //返回对应状态页面
        return view('test.create');
    }
    //表单提交处理
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'username' => 'required|max:1',
        ]);

        if($validator->fails()){
            return back()->withErrors($validator);
        }
        return '成功';
    }
}
