<?php

/*
 * 缓存学习
 */

namespace App\Http\Controllers\Demo;
use App\Http\Controllers\Common\BaseController;
use Illuminate\Support\Facades\Cache;
/**
 * Description of CacheController
 * Date 2019年4月18日 15:56:53
 * @author Carter
 */
class CacheController extends BaseController{
    /******
     *  设置获取缓存
     * *******/
    public function index(){
        Cache::put('name','zhang',10);
        echo '设置name'."<br>";
        if(Cache::has('name')){
            $name = Cache::get('name');
        }else{
            $name = 1;
        }
        echo '获取name'.$name."<br>";
        //自增自减
        Cache::put('age',20,10);
        echo '设置年龄20'."<br>";
        Cache::increment('age',2); //自增2，默认自增1
        echo '年龄自增为'.Cache::get('age')."<br>";
        Cache::decrement('age');
        echo '年龄自减为'.Cache::get('age')."<br>";
        //获取和存储（缓存数据不存在时获取数据并存储）
        $sex = Cache::remember('sex',10,function(){
            return 1;
        });
        echo '存储并获取的sex值为:'.Cache::get('sex')."<br>";
        //获取和删除
        echo '获取后并删除sex'.Cache::pull('sex')."<br>";
        //存储数据设置过期时间
        $expiresAt = now()->addMinute(60);
        Cache::put('sex',2,$expiresAt);
        //Cache::put('sex',3,100);  //100分钟
        echo '设置过期时间为60分钟后'.Cache::get('sex')."<br>";
        //只存储没有的数据，类似于setnx
        $bool = Cache::add('hei',3,20);
        echo '存储hei是否成功'.$bool;
        //数据永久存储
        Cache::forever('wode','a');
        //缓存删除
        Cache::forget('wode');
        echo '永久存储wode,然后删除掉'."<br>";
        //清空所有缓存
        Cache::flush();
        echo "flush清空所有缓存,不考虑前缀"."<br>";
        //缓存标记（不支持file和database驱动）
        //cache关于tags
        Cache::tags(['people','name'])->put('susan','123',10);
        Cache::tags(['people','age'])->put('susan','12',10);
        //1,获取：基本可以确定tags的数组内数据为合并后组成的唯一值，即不管是单独name，或者people，或者name,people都是无法得到值的,有点像并
        //echo Cache::tags(['people','name'])->get('susan')."<br>";
        //2,清除：基本可以确定tags数组内的任意键被清除，所有关于这个键值存在的tags都会被清空,和获取数组tags完全是相反的，有点像或
        Cache::tags(['people','name'])->flush();
        $susan = Cache::tags(['people','name'])->get('susan');
        echo 'peopel_name的susan值为:'.$susan."<br>";
        $susan = Cache::tags(['people','age'])->get('susan');
        echo 'peopel_age的susan值为:'.$susan."<br>";
        //3:tags内单个数组或字符
        Cache::tags('name')->put('susan','333',10);
        echo '标记name的susan值为:'.Cache::tags('name')->get('susan')."<br>";
        exit;
    }
    
}
