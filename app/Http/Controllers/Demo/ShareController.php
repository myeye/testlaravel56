<?php

/*
 * 分享功能控制器(目前微信，后续陆续可增加其他第三方分享)
 */
namespace App\Http\Controllers\Demo;
use App\Http\Controllers\Common\BaseController;
use Illuminate\Http\Request;
/**
 * Description of ShareController
 * Date 2018年12月4日 13:55:25
 * @author Carter
 */
class ShareController extends BaseController{
    /*****
     * summery  实时输出二维码接口
     * tag 公共使用
     * @param $url    string required  二维码内url地址
     * @param $size   int    optional 二维码尺寸,默认3
     * @param $margin int    optional 二维码白边宽度,默认4
     * @return mixed
     * ********/
    public function qrcodeOutput(Request $request){
        $url = $request->input('url','');
        $size = $request->input('size',3);
        $margin = $request->input('margin',4);
        \QRcode::png($url,false,QR_ECLEVEL_L,$size,$margin);
    }
    
    /*******
     * summery 微信服务器验证及消息处理返回
     * tag  微信接口
     * @return mixed
     * ********/
    public function wechatValid(){
        $app = app('wechat.official_account');  //初始化公众号服务
        //post消息处理返回
        $app->server->push(function($message){  
            return "欢迎关注此公众号，此为测试返回内容";
        });
        //get验证并返回echostr
        $response = $app->server->serve();  
        return $response;
    }
    
    /********
     * summery 获取使用jssdk需要的微信公众号配置信息
     * tag  微信接口
     * @param url string  当前url（默认读取当前url，通常不需要）
     * @return mixed
     */
    public function getJssdk(Request $request){
        $app = app('wechat.official_account');  //初始化公众号服务
        //当前url设置
        $url = $request->input('url','');
        if($url){
            $app->jssdk->setUrl($url);  //设置当前url
        }
        //生成jssdk的config信息
        $jssdkPackage = $app->jssdk->buildConfig(['updateAppMessageShareData','updateTimelineShareData'], $debug = false, $beta = false, $json = false);
        return $this->resultData('$_0',$jssdkPackage);
    }
}
