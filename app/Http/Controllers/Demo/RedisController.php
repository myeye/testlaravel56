<?php

/**
 * redis处理
 **/

namespace App\Http\Controllers\Demo;

use App\Http\Controllers\Common\BaseController;
use Illuminate\Support\Facades\Redis;

/**
 * Description of RedisController
 * Date 2019年5月10日 20:11:32
 * @author Carter
 */
class RedisController extends BaseController
{
    /***
     * 添加队列
     */
    public function addList()
    {
        Redis::lpush('test_list', 'cc');
        Redis::expire('test_list', 600);
        return '设置成功';
    }

    /**
     * 读取队列
     */
    public function getList()
    {
         $len = Redis::llen('test_list');
         for($i=0;$i<$len;$i++){
             $value = Redis::rpop('test_list');
             file_put_contents('11.txt',$value,FILE_APPEND);
             Redis::setex('cc'.$i,300,$value);
             sleep(10);
         }
         return $len;
    }
}
