<?php
/*****
 * ValidateController.php
 * 测试表单验证
 * Created on 2020/4/15 18:01
 * Create by Carter
 ****/

namespace App\Http\Controllers\Demo;


use App\Http\Controllers\Common\BaseController;
use App\Http\Requests\IndexPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidateController extends BaseController
{
    /****
     * 使用表单请求类验证
     */
    public function index(IndexPost $request){

        return $this->resultData('$_0');
    }

    /**
     * 方法内表单验证
     */
    public function index1(Request $request){
        $validate  = Validator::make($request->all(),
            [
                'name' => 'required|integer|gt:'
            ],
            [
                'name.required' => ':attribute缺失',
            ],
            [
                'name' => '姓名',
            ])->validate();
    }
}
