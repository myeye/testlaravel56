<?php

/*
 * orm功能测试控制器
 */
namespace App\Http\Controllers\Demo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Model\Demo\User;
use App\Http\Model\Demo\Assets;
/**
 * Description of OrmController
 * Date 2019年1月13日 15:14:47
 * @author Carter
 */
class OrmController extends ConTroller{
    /*****
     * summery  测试一对一关联操作
     * tag 公共使用
     * @return mixed
     * ********/
    public function testHasOne(Request $request){
        $res = User::find(1)->assets;
        return $res;
    }

    public function testBelongsTo(Request $request){
        $res = Assets::find(1)->user;
        return $res;
    }
    
}
