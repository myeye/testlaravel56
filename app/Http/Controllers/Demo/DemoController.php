<?php

/*
 * 测试功能类
 */

namespace App\Http\Controllers\Demo;
use App\Http\Controllers\Common\BaseController;
use Carter\Bank\Bankcard;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
/**
 * Description of DemoController
 * Date 2018年12月6日 15:07:52
 * @author Carter
 */
class DemoController extends BaseController{
    //测试控制器
    public function __invoke($id){  //单个行为控制器
        //return $id.'测试单个行为控制器';
        return view('welcome',['id'=>$id]);
    }
    
    //测试获取json返回数据
    public function index1(){
        return $this->resultData('$_1');
    }
    //测试检查参数有效不为空
    public function index2(){
       $checkRes = $this->checkParams(['name','age','sex']);
       if($checkRes !== true){
           return $checkRes;
       }
    }
    //测试配置信息
    public function index3(){
        //获取当前环境
        //$appEnv = App::environment();
        //return $this->resultData('$_0',[$appEnv]);
        //获取config设置
        $timezone = config('app.timezone');
        return $this->resultData('$_0',[$timezone]);
    }
    
    //测试uuid方法
    public function index4(){
        return (string)Str::uuid();
        //return (string)Str::orderedUuid();
        //dd(Str::orderedUuid());
    }
    //测试http请求
    public function index5(Request $request){
        //return $request->path();  //请求路径
//        if($request->is('api/*')){
//            return 'api模块';
//        }
        //return $request->url();  //获取请求的 URL,不带查询字符串
        //return $request->fullUrl();  ////获取请求的 URL,带查询字符串
//        if($request->isMethod('post')){  //获取请求方法
//            return 'post 请求';
//        }else{
//            return 'get请求';
//        }
        //return $request->all();//获取所有输入数据
        //return $request->query('name','sale');//从查询字符串获取输入
        //return $request->only(['name','age']);//获取部分输入数据
        //return $request->except(['name','age']);//获取部分输入数据
//        if($request->has('name')){//确定是否存在输入值
//            return '存在name';
//        }else{
//            return '不存在name';
//        }
        if($request->filled('name')){//确定请求中是否存在值并且不为空
            return '存在name且不为空';
        }
        
        
    }
    //测试响应
    public function index6(){
        return 'response aaa';   //返回字符串
        //return [1,2,3];   //返回数组
        //return response('hello world',200)->header('Content-Type', 'text/plain')->cookie('name','zhang');//响应对象
        //return redirect('welcome');  //重定向
        //return back()->withInput();//返回之前位置（必须是使用了web中间件,因为需要session）
        //return redirect()->route('welcome');//重定向至命名路由  
        //return redirect()->away('http://www.baidu.com');//重定向到外部域
        //return response()->view('welcome');//视图响应
//        return response()->json([
//            'name' => ' carter',
//            'age' => 29,
//        ]);//json响应
        //$pathToFile = public_path('111.txt');
        //return response()->download($pathToFile)->deleteFileAfterSend(true);//文件下载,加上deleteFileAfterSend会在下载后删除原文件
//        return response()->streamDownload(function(){
//            echo file_get_contents('http://172.16.55.44:9090/examples/commission_201801184444.pdf');
//        },'laravel.pdf');//流式下载
        //return response()->file($pathToFile);//文件响应预览
    }
    
   //测试url
    public function index7(){
        //return url('/post/1?aa=bb');  //生成基础 URL
        //return url()->current();  //获取不含查询字符串的当前url
        //return url()->full();  //获取含查询字符串的当前url
        return url()->previous();  //获取上一个请求的完整url
    }
    
    //测试session
    public function index8(){
        //PS:在api请求时不要用$request->session()-操作，因为laravel需要web路由过来才允许request设置session，用session()->操作即可
        //session()->put('name', 'carter');
        //return session()->get('name','aa');
        //session(['name' => 'zhang']);  //设置session
        //return session('name');
        //$data = session()->all();  //获取所有session
        //return $data;
//       if(session()->has('name')){ //判断session是否存在某个值
//           echo '此session存在且不为null';
//       }else{
//           echo '此session不存在或为null';
//       }
//        if(session()->exists('name')){ //判断session是否存在某个值,即使为null
//           echo '此session存在';
//        }else{
//           echo '此session存在';
//        }
        //session()->put('name','carter');
//        session()->push('user.age', '2');//在 Session 数组中保存数据
//        return session()->get('user');
//        session()->pull('name');  //检索 & 删除一个项目
//        return session('name');
        //闪存数据
        //session()->flash('status','123');
        //return session('status');
        //删除session所有数据
        //session()->flush();
        //闪存所有session数据给所有请求
//        session()->put('name','carter');
//        session()->put('age','29');
//        session()->reflash();
        //return session('age');
        //删除数据
        session()->forget('age');
        return session('age');
    }
    
    //测试异常
    public function index9(){
        try {
            if($aa > 0){
                return 1;
            }
        } catch (\Exception $ex) {
            report($ex);  //将错误写在了laravel.log文件
            return 3;
        }
        return 2;
    }


    /**生成二维码**/
    public function index10(){
        $back_color = 0xFFFF00;
        $fore_color = 0xFF00FF;
        ob_start();//开启缓冲区
        \QRcode::png('some othertext 1234', false, 'h', 20, 1, false, $back_color, $fore_color);
        $img = ob_get_contents();//获取缓冲区内容
        ob_end_clean();//清除缓冲区内容
        $imgInfo = 'data:png;base64,' . chunk_split(base64_encode($img));//转base64
        return $imgInfo;
    }

    //测试银行卡查询依赖
    public function testBankcard(){
        $bank = new Bankcard();
        $res = $bank->getBankInfo('icbc');
        return $res;
    }

    //测试base64视频
    public function videoToBase64(){
        $res = file_get_contents(public_path('1cd4943edec11a95bc06fd08a1d71b54_0.mp4'));
        $base_res =  base64_encode($res);
        file_put_contents('111.mp4',base64_decode($base_res));
        return 1;
    }

}
