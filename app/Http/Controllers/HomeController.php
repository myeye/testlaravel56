<?php
/*****
 * HomeController.php
 * home
 * Created on 2019/11/13 11:01
 * Create by Carter
 ****/

namespace App\Http\Controllers;


use App\Http\Controllers\Common\BaseController;

class HomeController extends BaseController
{
    public function index(){
        return 'hello world';
    }
}
