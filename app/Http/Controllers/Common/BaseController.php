<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    protected $server;
    protected $common_user_id;

    public function __construct()
    {
        $this->common_user_id = 0;
        if (\Auth::guard('web')->check()) {
            $result = \Auth::guard('web')->user();
            $this->common_user_id = $result->user_id;
        }
    }

    /**
     * 封装输出
     * @param array $res 格式数组 ['$_0', data] 或 [["0", "操作成功"], data]
     */
    protected function returnData($res = [])
    {
        return $this->resultData($res[0]??'$_0', $res[1]??[]);
    }

    /**
     * @todo 格式化返回数据
     * @param array $data     数据
     * @param array $code  错误代码数组
     * @param(header) string $lang  语言类型
     * @return mixed
     */
    public function resultData($code = ["0", "操作成功"], $data = []){
        //根据语言切换语言状态码类
        $lang_arr = [
            'Cn',   //中文
            'En',   //英文
        ];
        $lang = request()->header('lang','Cn');
        if(!in_array($lang, $lang_arr)){
            $lang = 'Cn';
        }
        //调用语言状态码类
        if(is_string($code)){
            @eval('$code = \\App\\Utils\\StatusCode\\StatusCodeFront'.$lang.'::'.$code.';');
        }
        //组合返回数组
        $result['code'] = $code[0];
        $result['msg'] = $code[1];
        if(!is_array($data)){
            $data = [];
        }
        $result['data'] = $data;
        return response()->json($result)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        //JsonResponse::create($result)->setEncodingOptions(JSON_UNESCAPED_UNICODE)->send();
        exit;
    }
    
    /**
     * 检验指定参数是否存在或有效
     * @param array $data
     */
    protected function checkParams($data = []){
        $data_num = count($data);
        //循环处理
        foreach ($data as $v) {
            if(!request()->filled($v)){ //不存在或存在为空
                return $this->resultData('$_102',['参数:'.$v.'缺失']);
            }
        }

        return true;
    }
    
}
